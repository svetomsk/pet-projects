package ru.svetomsk

import com.github.kotlintelegrambot.entities.ChatId
import io.kotest.core.spec.style.FunSpec
import io.mockk.coJustRun
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.withContext
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.conversation.ConversationContext
import ru.svetomsk.telegram.conversation.step.MessageStep

class ApplicationTest : FunSpec() {

    init {
        test("messageStepTest").config(blockingTest = true) {
            val chatIdMock = mockk<ChatId>()
            val context = currentCoroutineContext().plus(ConversationContext(chatIdMock))
            withContext(context) {
                val botMock = mockk<TelegramBot>()
                coJustRun { botMock.sendMessage(any()) }
                val message = "message"
                val sut = MessageStep(botMock, message)
                sut.perform()
                coVerify { botMock.sendMessage(message) }
            }
        }
    }

}
