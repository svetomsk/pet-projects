package ru.svetomsk.model

import kotlinx.serialization.Serializable

@Serializable
data class Student(val id: Long = -1, var name: String?)

@Serializable
data class GetStudentsResponse(val students: List<Student>)