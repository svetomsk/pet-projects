package ru.svetomsk.model

import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.LocalTime
import kotlinx.serialization.Serializable

@Serializable
data class LessonDto(var time: LocalTime, var studentId: Long, var dayOfWeek: DayOfWeek)

@Serializable
data class ScheduleForDay(val dayOfWeek: DayOfWeek, val lessons: List<LessonDto>)

@Serializable
data class ScheduleForWeek(val lessons: Map<DayOfWeek, ScheduleForDay>)