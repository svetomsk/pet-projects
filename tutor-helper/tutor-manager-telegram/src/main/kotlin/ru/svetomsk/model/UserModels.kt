package ru.svetomsk.model

import kotlinx.serialization.Serializable

@Serializable
data class BotUserDto(val chatId: Long)