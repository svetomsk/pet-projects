package ru.svetomsk.service

import org.koin.core.module.Module
import org.koin.dsl.module
import ru.svetomsk.service.contract.IMessageConverter
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.service.contract.IUserService
import ru.svetomsk.service.impl.ServerScheduleService
import ru.svetomsk.service.impl.SimpleMessageConverter
import ru.svetomsk.service.impl.UserService

fun serviceModules(): Module {
    return module {
        single<IMessageConverter> { SimpleMessageConverter() }
        single<IScheduleService> { ServerScheduleService() }
        single<IUserService> { UserService() }
    }
}