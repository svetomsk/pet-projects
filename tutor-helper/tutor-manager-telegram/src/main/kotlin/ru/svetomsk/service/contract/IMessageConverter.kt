package ru.svetomsk.service.contract

import ru.svetomsk.model.ScheduleForDay
import ru.svetomsk.model.ScheduleForWeek

interface IMessageConverter {
    fun convertScheduleForOneDay(schedule: ScheduleForDay): String
    fun convertScheduleForWeek(schedule: ScheduleForWeek): String
}