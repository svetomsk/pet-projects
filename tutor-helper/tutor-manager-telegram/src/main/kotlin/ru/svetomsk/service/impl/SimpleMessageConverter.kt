package ru.svetomsk.service.impl

import kotlinx.datetime.DayOfWeek
import ru.svetomsk.model.ScheduleForDay
import ru.svetomsk.model.ScheduleForWeek
import ru.svetomsk.service.contract.IMessageConverter
import java.time.DayOfWeek.*

class SimpleMessageConverter : IMessageConverter {
    override fun convertScheduleForOneDay(schedule: ScheduleForDay): String {
        return StringBuilder().apply {
            appendLine(schedule.dayOfWeek.translate())
            schedule.lessons.forEach {
                appendLine("${it.studentId} в ${it.time}")
            }
        }.toString()
    }

    override fun convertScheduleForWeek(schedule: ScheduleForWeek): String {
        return StringBuilder().apply {
//            appendLine("Расписание на неделю: ")
//            for (dayOfWeek in entries) {
//                schedule.lessons[dayOfWeek]?.let {
//                    append(convertScheduleForOneDay(it))
//                } ?: println("No schedule for $dayOfWeek")
//                appendLine()
//            }
        }.toString()
    }

    private fun DayOfWeek.translate(): String {
        return when (this) {
            MONDAY -> "Понедельник"
            TUESDAY -> "Вторник"
            WEDNESDAY -> "Среда"
            THURSDAY -> "Четверг"
            FRIDAY -> "Пятница"
            SATURDAY -> "Суббота"
            SUNDAY -> "Воскресенье"
        }
    }
}
