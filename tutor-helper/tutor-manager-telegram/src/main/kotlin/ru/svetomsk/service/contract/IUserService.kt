package ru.svetomsk.service.contract

import ru.svetomsk.model.BotUserDto

interface IUserService {
    fun createUser(chatId: Long): BotUserDto
}