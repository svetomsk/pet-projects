package ru.svetomsk.service.impl

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.httpclient.UserClient
import ru.svetomsk.model.BotUserDto
import ru.svetomsk.service.contract.IUserService

class UserService : IUserService, KoinComponent {
    private val userClient: UserClient by inject<UserClient>()
    override fun createUser(chatId: Long): BotUserDto {
        return userClient.createUser(BotUserDto(chatId))
    }
}