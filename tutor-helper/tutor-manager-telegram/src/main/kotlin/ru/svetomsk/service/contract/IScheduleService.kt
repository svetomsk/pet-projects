package ru.svetomsk.service.contract

interface IScheduleService {
    fun getScheduleForToday(): String

    fun getScheduleForWeek(): String

    fun getScheduleForTomorrow(): String
}