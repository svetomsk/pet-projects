package ru.svetomsk.service.impl

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.httpclient.ScheduleClient
import ru.svetomsk.service.contract.IMessageConverter
import ru.svetomsk.service.contract.IScheduleService

open class ServerScheduleService : KoinComponent, IScheduleService {
    private val messageConverter: IMessageConverter by inject()
    private val scheduleClient: ScheduleClient by inject()
    override fun getScheduleForToday(): String {
        val serverResponse = scheduleClient.getScheduleForToday()
        return messageConverter.convertScheduleForOneDay(serverResponse)
    }

    override fun getScheduleForWeek(): String {
        val serverResponse = scheduleClient.getScheduleForWeek()
        return messageConverter.convertScheduleForWeek(serverResponse)
    }

    override fun getScheduleForTomorrow(): String {
        val serverResponse = scheduleClient.getScheduleForTomorrow()
        return messageConverter.convertScheduleForOneDay(serverResponse)
    }


}