package ru.svetomsk.config

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper

object JacksonConfiguration {
    val defaultMapper = jacksonObjectMapper()

    init {
        defaultMapper.registerModule(JavaTimeModule())
    }
}