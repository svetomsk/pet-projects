package ru.svetomsk.config

import org.koin.core.module.Module
import org.koin.dsl.module

interface ITelegramBotConfig {
    fun getToken(): String
}

fun telegramBotConfig(): Module {
    return module {
        single<ITelegramBotConfig> { LocalTelegramBotConfig() }
    }
}