package ru.svetomsk.httpclient

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import io.ktor.serialization.jackson.*
import kotlinx.coroutines.runBlocking
import org.koin.core.module.Module
import org.koin.dsl.module
import ru.svetomsk.config.JacksonConfiguration
import ru.svetomsk.model.*

open class HttpClientHelper {
    protected val client = HttpClient(CIO) {
        install(Logging)
        install(ContentNegotiation) {
            register(ContentType.Application.Json, JacksonConverter(JacksonConfiguration.defaultMapper))
        }
    }

    protected inline fun <reified T> makeBlockingCall(
        url: String,
        httpMethod: HttpMethod = HttpMethod.Get,
        body: Any = ""
    ): T {
        return runBlocking {
            return@runBlocking client.request(url) {
                method = httpMethod
                if (method != HttpMethod.Get) {
                    contentType(ContentType.Application.Json)
                    setBody(JacksonConfiguration.defaultMapper.writeValueAsString(body))
                }
            }.body()
        }
    }
}

class ScheduleClient : HttpClientHelper() {
    // TODO: move server address to properties
    private val serverAddress = "http://localhost:8080/schedule"

    fun getScheduleForToday(): ScheduleForDay {
        return makeBlockingCall<ScheduleForDay>("$serverAddress/today")
    }

    fun getScheduleForTomorrow(): ScheduleForDay {
        return makeBlockingCall<ScheduleForDay>("$serverAddress/tomorrow")
    }

    fun getScheduleForWeek(): ScheduleForWeek {
        return makeBlockingCall<ScheduleForWeek>("$serverAddress/week")
    }

    fun createLesson(lessonDto: LessonDto) {
        makeBlockingCall<Any>("$serverAddress/lesson", HttpMethod.Put, lessonDto)
    }
}

class UserClient : HttpClientHelper() {
    private val serverAddress = "http://localhost:8080/users"

    fun createUser(botUserDto: BotUserDto): BotUserDto {
        return makeBlockingCall<BotUserDto>(serverAddress, HttpMethod.Post, botUserDto)
    }
}

class StudentClient : HttpClientHelper() {
    private val serverAddress = "http://localhost:8080/students"

    fun createStudent(student: Student) {
        makeBlockingCall<Any>(serverAddress, HttpMethod.Put, student)
    }

    fun getStudents(): List<Student> {
        return makeBlockingCall<GetStudentsResponse>(serverAddress, HttpMethod.Get).students
    }

    fun removeStudent(studentId: Long) {
        makeBlockingCall<Any>("$serverAddress/$studentId", HttpMethod.Delete)
    }
}

fun httpClientsModules(): Module {
    return module {
        single { ScheduleClient() }
        single { UserClient() }
        single { StudentClient() }
    }
}