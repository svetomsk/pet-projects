package ru.svetomsk

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.logger.slf4jLogger
import ru.svetomsk.config.telegramBotConfig
import ru.svetomsk.httpclient.httpClientsModules
import ru.svetomsk.service.serviceModules
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.telegramModules

object TelegramApplication : KoinComponent {
    private val telegramBot: TelegramBot by inject()
    fun start() {
        configureApplication()
        telegramBot.startBot()
    }

    private fun configureApplication() {
        startKoin {
            slf4jLogger(level = Level.DEBUG)
            modules(
                telegramBotConfig(),
                telegramModules(),
                httpClientsModules(),
                serviceModules(),
            )
        }
    }
}

fun main() {
    TelegramApplication.start()
}
