package ru.svetomsk.telegram.conversation.step

import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.model.Student
import ru.svetomsk.telegram.TelegramBot

class AddStudentStep(
    private val telegramBot: TelegramBot,
    private val student: Student,
    private val studentsClientHelper: StudentClient
) : ConversationStep {
    override suspend fun perform(userMessage: String) {
        studentsClientHelper.createStudent(student)
        telegramBot.sendMessage("Student successfully created")
    }
}