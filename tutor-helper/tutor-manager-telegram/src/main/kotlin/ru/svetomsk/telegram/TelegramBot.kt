package ru.svetomsk.telegram

import com.github.kotlintelegrambot.Bot
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.Dispatcher
import com.github.kotlintelegrambot.dispatcher.message
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.BotCommand
import com.github.kotlintelegrambot.entities.ChatId
import com.github.kotlintelegrambot.entities.Message
import com.github.kotlintelegrambot.extensions.filters.Filter
import com.github.kotlintelegrambot.logging.LogLevel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.withContext
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.module.Module
import org.koin.dsl.module
import ru.svetomsk.config.ITelegramBotConfig
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.service.contract.IUserService
import ru.svetomsk.telegram.Commands.ADD_LESSON
import ru.svetomsk.telegram.Commands.ADD_STUDENT
import ru.svetomsk.telegram.Commands.LIST_STUDENTS
import ru.svetomsk.telegram.Commands.START
import ru.svetomsk.telegram.Commands.TODAY
import ru.svetomsk.telegram.Commands.TOMORROW
import ru.svetomsk.telegram.Commands.WEEK
import ru.svetomsk.telegram.conversation.*

object Commands {
    val START = BotCommand("/start", "Start communication")
    val TOMORROW = BotCommand("/tomorrow", "Schedule for tomorrow")
    val WEEK = BotCommand("/week", "Schedule for full week")
    val TODAY = BotCommand("/today", "Schedule for today")
    val ADD_STUDENT = BotCommand("/add_student", "Add new student")
    val LIST_STUDENTS = BotCommand("/list_students", "Show list of students")
    val ADD_LESSON = BotCommand("/add_lesson", "Add new lesson")
}

class TelegramBot : KoinComponent {
    private val scheduleService: IScheduleService by inject<IScheduleService>()
    private val userService: IUserService by inject<IUserService>()
    private val telegramConfig: ITelegramBotConfig by inject<ITelegramBotConfig>()
    private lateinit var activeConversation: BasicConversation

    private var bot: Bot = bot {
        token = telegramConfig.getToken()
        timeout = 30
        logLevel = LogLevel.Error

        addCommandHandlers()
    }.apply {
        setMyCommands(
            listOf(
                START,
                TODAY, TOMORROW, WEEK,
                ADD_STUDENT, LIST_STUDENTS,
                ADD_LESSON
            )
        )
    }

    private fun Bot.Builder.addCommandHandlers() {
        dispatch {
            text(START.command) {
                userService.createUser(message.getChatId().id)
                bot.sendMessage(message.getChatId(), "Hi, I can help you with students schedule")
            }

            scheduleCommands()
            studentCommands()
            lessonCommands()

            message(Filter.Text) {
                runWithTelegramContext(message) {
                    activeConversation.makeStep(message.text!!)
                }
            }
        }
    }

    private fun Dispatcher.scheduleCommands() {
        text(TOMORROW.command) {
            bot.sendMessage(message.getChatId(), scheduleService.getScheduleForTomorrow())
        }

        text(WEEK.command) {
            bot.sendMessage(message.getChatId(), scheduleService.getScheduleForWeek())
        }

        text(TODAY.command) {
            bot.sendMessage(message.getChatId(), scheduleService.getScheduleForToday())
        }
    }

    private fun Dispatcher.studentCommands() {
        text(ADD_STUDENT.command) {
            runWithTelegramContext(message) {
                activeConversation = AddStudentConversation()
                activeConversation.makeStep()
            }
        }

        text(LIST_STUDENTS.command) {
            runWithTelegramContext(message) {
                activeConversation = ListStudentsConversation()
                activeConversation.makeStep()
            }
        }
    }

    private fun Dispatcher.lessonCommands() {
        text(ADD_LESSON.command) {
            runWithTelegramContext(message) {
                activeConversation = AddLessonConversation()
                activeConversation.makeStep()
            }
        }
    }

    fun startBot() {
        bot.startPolling()
    }

    suspend fun sendMessage(message: String) {
        currentCoroutineContext()[ConversationContext.Key]?.run {
            bot.sendMessage(chatId, message)
        }
    }
}

suspend fun runWithTelegramContext(message: Message, block: suspend CoroutineScope.() -> Any) {
    withContext(currentCoroutineContext().plus(ConversationContext(message.getChatId())), block)
}

fun Message.getChatId() = ChatId.fromId(chat.id)

fun telegramModules(): Module {
    return module {
        single { TelegramBot() }
    }
}