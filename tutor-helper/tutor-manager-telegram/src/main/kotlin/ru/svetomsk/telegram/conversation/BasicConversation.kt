package ru.svetomsk.telegram.conversation

import ru.svetomsk.telegram.conversation.step.ConversationStep

abstract class BasicConversation {
    private val steps = mutableListOf<ConversationStep>()
    private var stepNumber = -1
    private fun isFinished() = stepNumber + 1 == steps.size
    suspend fun makeStep(userMessage: String = ""): Boolean {
        if (stepNumber >= 0) {
            steps[stepNumber].saveAnswer(userMessage)
        }
        if (!isFinished()) {
            stepNumber++
            val currentStep = steps[stepNumber]
            currentStep.perform(userMessage)
        } else {
            println("Conversation ${this.javaClass.simpleName} is finished")
        }
        return isFinished()
    }

    protected fun addStep(step: ConversationStep) {
        steps.add(step)
    }
}