package ru.svetomsk.telegram.conversation

import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.LocalTime
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.httpclient.ScheduleClient
import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.model.LessonDto
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.conversation.step.AddLessonStep
import ru.svetomsk.telegram.conversation.step.MessageStep
import ru.svetomsk.telegram.conversation.step.SelectUserByNumberStep

class AddLessonConversation : BasicConversation(), KoinComponent {
    private val studentClient: StudentClient by inject()
    private val scheduleClient: ScheduleClient by inject()
    private val telegramBot: TelegramBot by inject()

    init {
        val lessonDto = LessonDto(LocalTime.fromSecondOfDay(100), -1, DayOfWeek.THURSDAY)
        // provide user select
        addStep(SelectUserByNumberStep(telegramBot, studentClient) { studentNumber ->
            lessonDto.studentId = studentNumber.toLong()
        })
        // ask for day of week
        addStep(MessageStep(telegramBot, "Enter day of week for this lesson") { dayOfWeek ->
            lessonDto.dayOfWeek = DayOfWeek.valueOf(dayOfWeek.uppercase())
        })
        // ask for lesson time
        addStep(MessageStep(telegramBot, "Enter time of this lesson") { lessonTime ->
            lessonDto.time = LocalTime.parse(lessonTime)
        })
        // store lesson on server
        addStep(AddLessonStep(lessonDto, scheduleClient))
    }
}