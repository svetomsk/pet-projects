package ru.svetomsk.telegram.conversation.step

interface ConversationStep {
    suspend fun perform(userMessage: String = "")

    fun saveAnswer(userMessage: String = "") {

    }
}
