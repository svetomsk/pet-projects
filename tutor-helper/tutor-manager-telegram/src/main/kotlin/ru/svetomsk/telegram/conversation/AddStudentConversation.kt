package ru.svetomsk.telegram.conversation

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.model.Student
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.conversation.step.AddStudentStep
import ru.svetomsk.telegram.conversation.step.MessageStep

class AddStudentConversation : BasicConversation(), KoinComponent {
    private val studentClient: StudentClient by inject()
    private val telegramBot: TelegramBot by inject()

    init {
        val student = Student(name = "")
        val messageStep = MessageStep(telegramBot, "Enter the name of student") { value -> student.name = value }
        addStep(messageStep) // ask for student name
        addStep(AddStudentStep(telegramBot, student, studentClient)) // save student on server
    }
}