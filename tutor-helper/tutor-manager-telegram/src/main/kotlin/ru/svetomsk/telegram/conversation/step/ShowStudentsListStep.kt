package ru.svetomsk.telegram.conversation.step

import kotlinx.coroutines.currentCoroutineContext
import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.conversation.ConversationContext

class ShowStudentsListStep(
    private val telegramBot: TelegramBot,
    private val studentClient: StudentClient
) : ConversationStep {
    private val studentNumberToId = mutableMapOf<Int, Long>()
    override suspend fun perform(userMessage: String) {
        currentCoroutineContext()[ConversationContext.Key]?.run {
            val students = studentClient.getStudents()
            val message = StringBuilder("Your students: \n")
            students.forEachIndexed { index, student ->
                message
                    .append(index)
                    .append(" - ")
                    .append(student.name)
                    .append("\n")
                studentNumberToId[index] = student.id
            }
            telegramBot.sendMessage(message.toString())
        }
    }

    fun getStudentIdByNumber(number: Int) = studentNumberToId[number]
}