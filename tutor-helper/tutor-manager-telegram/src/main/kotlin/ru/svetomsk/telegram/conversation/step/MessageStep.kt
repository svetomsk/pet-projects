package ru.svetomsk.telegram.conversation.step

import ru.svetomsk.telegram.TelegramBot

class MessageStep(
    private val telegramBot: TelegramBot,
    private val message: String,
    private val callbackSetter: (String) -> Unit = { _ -> }
) : ConversationStep {
    override suspend fun perform(userMessage: String) {
        telegramBot.sendMessage(message)
    }

    override fun saveAnswer(userMessage: String) {
        callbackSetter.invoke(userMessage)
    }
}