package ru.svetomsk.telegram.conversation

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.telegram.TelegramBot
import ru.svetomsk.telegram.conversation.step.ShowStudentsListStep

class ListStudentsConversation : BasicConversation(), KoinComponent {
    private val studentClient: StudentClient by inject()
    private val telegramBot: TelegramBot by inject()
    init {
        addStep(ShowStudentsListStep(telegramBot, studentClient))
    }
}