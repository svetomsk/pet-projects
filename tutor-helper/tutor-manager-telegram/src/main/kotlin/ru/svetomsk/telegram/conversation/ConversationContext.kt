package ru.svetomsk.telegram.conversation

import com.github.kotlintelegrambot.entities.ChatId
import kotlin.coroutines.AbstractCoroutineContextElement
import kotlin.coroutines.CoroutineContext

data class ConversationContext(
    val chatId: ChatId
) : AbstractCoroutineContextElement(ConversationContext) {
    companion object Key : CoroutineContext.Key<ConversationContext>
}