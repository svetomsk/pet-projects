package ru.svetomsk.telegram.conversation.step

import ru.svetomsk.httpclient.ScheduleClient
import ru.svetomsk.model.LessonDto

class AddLessonStep(
    val lessonDto: LessonDto,
    val lessonClient: ScheduleClient
) : ConversationStep {
    override suspend fun perform(userMessage: String) {
        lessonClient.createLesson(lessonDto)
        println("Lesson created")
    }
}