package ru.svetomsk.telegram.conversation.step

import ru.svetomsk.httpclient.StudentClient
import ru.svetomsk.telegram.TelegramBot

class SelectUserByNumberStep(
    telegramBot: TelegramBot,
    studentClient: StudentClient,
    private val callbackAnswer: (String) -> Unit
) : ConversationStep {
    private val showStudentsListStep = ShowStudentsListStep(telegramBot, studentClient)
    private val messageStep = MessageStep(telegramBot, "Select number from list above")
    override suspend fun perform(userMessage: String) {
        showStudentsListStep.perform()
        messageStep.perform()
    }

    override fun saveAnswer(userMessage: String) {
        showStudentsListStep.getStudentIdByNumber(userMessage.toInt())?.run {
            callbackAnswer.invoke(this.toString())
        } ?: throw RuntimeException("No mapping for number $userMessage")
    }
}