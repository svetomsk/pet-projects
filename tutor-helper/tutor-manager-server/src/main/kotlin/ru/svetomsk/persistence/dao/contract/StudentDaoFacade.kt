package ru.svetomsk.persistence.dao.contract

import ru.svetomsk.model.StudentDto

interface StudentDaoFacade {
    suspend fun addStudent(studentDto: StudentDto): StudentDto
    suspend fun findStudentById(id: Long): StudentDto?
    suspend fun findStudentsByName(name: String): List<StudentDto>
    suspend fun findAllStudents(): List<StudentDto>

    suspend fun deleteStudent(studentId: Long): Boolean
    suspend fun updateStudent(studentId: Long, studentDto: StudentDto): Boolean
}