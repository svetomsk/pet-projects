package ru.svetomsk.persistence

import org.koin.core.module.Module
import org.koin.dsl.module
import ru.svetomsk.persistence.dao.contract.LessonDaoFacade
import ru.svetomsk.persistence.dao.contract.StudentDaoFacade
import ru.svetomsk.persistence.dao.contract.UsersDaoFacade
import ru.svetomsk.persistence.dao.impl.LessonDaoFacadeImpl
import ru.svetomsk.persistence.dao.impl.StudentDaoFacadeImpl
import ru.svetomsk.persistence.dao.impl.UsersDaoFacadeImpl

fun persistenceModules(): Module {
    return module {
        single<UsersDaoFacade> { UsersDaoFacadeImpl() }
        single<LessonDaoFacade> { LessonDaoFacadeImpl() }
        single<StudentDaoFacade> { StudentDaoFacadeImpl() }
    }
}