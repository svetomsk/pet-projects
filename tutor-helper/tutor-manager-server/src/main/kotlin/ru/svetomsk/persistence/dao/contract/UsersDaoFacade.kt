package ru.svetomsk.persistence.dao.contract

import ru.svetomsk.model.BotUserDto


interface UsersDaoFacade {
    suspend fun save(userDto: BotUserDto): BotUserDto
    suspend fun allUsers(): List<BotUserDto>
    suspend fun findByChatId(chatId: Long): BotUserDto?
    suspend fun deleteUser(chatId: Long): Int
    suspend fun deleteAllUsers(): Int
}