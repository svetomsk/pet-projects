package ru.svetomsk.persistence.dao.contract

import kotlinx.datetime.DayOfWeek
import ru.svetomsk.model.LessonDto

interface LessonDaoFacade {
    suspend fun addLesson(lessonDto: LessonDto): LessonDto
    suspend fun removeLesson(lessonId: Long): Boolean
    suspend fun getLessonByDay(dayOfWeek: DayOfWeek): List<LessonDto>
    suspend fun updateLesson(lessonId: Long, lessonDto: LessonDto): Boolean

    suspend fun getAllLessons(): List<LessonDto>
}