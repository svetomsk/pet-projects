package ru.svetomsk.persistence.dao.impl

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import ru.svetomsk.model.StudentDto
import ru.svetomsk.persistence.DatabaseFactory.dbQuery
import ru.svetomsk.persistence.StudentEntity
import ru.svetomsk.persistence.dao.contract.StudentDaoFacade
import ru.svetomsk.persistence.dao.resultRowToStudentDto
import java.lang.RuntimeException

class StudentDaoFacadeImpl : StudentDaoFacade {
    override suspend fun addStudent(studentDto: StudentDto): StudentDto {
        return dbQuery {
            val insertStatement = StudentEntity.insert {
                it[name] = studentDto.name
                it[timezone] = "UTC"
            }
            insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToStudentDto)
                ?: throw RuntimeException("Unable to create user")
        }
    }

    override suspend fun findStudentById(id: Long): StudentDto? {
        return dbQuery {
            StudentEntity.select { StudentEntity.id eq id }
                .map(::resultRowToStudentDto)
                .singleOrNull()
        }
    }

    override suspend fun findStudentsByName(name: String): List<StudentDto> {
        return dbQuery {
            StudentEntity.select { StudentEntity.name.lowerCase() like "%" + name.lowercase() + "%" }
                .map(::resultRowToStudentDto)
        }
    }

    override suspend fun findAllStudents(): List<StudentDto> {
        return dbQuery {
            StudentEntity.selectAll()
                .map(::resultRowToStudentDto)
        }
    }

    override suspend fun deleteStudent(studentId: Long): Boolean {
        return dbQuery {
            StudentEntity.deleteWhere {
                this.id eq studentId
            }
        } > 0
    }

    override suspend fun updateStudent(studentId: Long, studentDto: StudentDto): Boolean {
        return dbQuery {
            StudentEntity.update({ StudentEntity.id eq studentId }) {
                it[name] = studentDto.name
            }
        } > 0
    }
}