package ru.svetomsk.persistence.dao

import kotlinx.datetime.toKotlinLocalTime
import org.jetbrains.exposed.sql.ResultRow
import ru.svetomsk.model.BotUserDto
import ru.svetomsk.model.LessonDto
import ru.svetomsk.model.StudentDto
import ru.svetomsk.persistence.LessonEntity
import ru.svetomsk.persistence.StudentEntity
import ru.svetomsk.persistence.UserEntity

fun resultRowToStudentDto(row: ResultRow) = StudentDto(
    id = row[StudentEntity.id],
    name = row[StudentEntity.name]
)

fun resultRowToLessonDto(row: ResultRow) = LessonDto(
    id = row[LessonEntity.id],
    time = row[LessonEntity.time].toKotlinLocalTime(),
    studentId = row[LessonEntity.studentId],
    dayOfWeek = row[LessonEntity.dayOfWeek]
)


fun resultRowToBotUserDto(row: ResultRow) = BotUserDto(
    chatId = row[UserEntity.chatId]
)