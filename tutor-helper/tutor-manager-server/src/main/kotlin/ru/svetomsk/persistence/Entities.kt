package ru.svetomsk.persistence

import kotlinx.datetime.DayOfWeek
import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.time

object UserEntity: Table() {
    private val id = long("id").autoIncrement()
    val chatId = long("chat_id")

    override val primaryKey = PrimaryKey(id)
}

object StudentEntity: Table() {
    val id = long("id").autoIncrement()
    val name = text("name")
    val timezone = text("timezone")

    override val primaryKey = PrimaryKey(id)
}

object LessonEntity: Table() {
    val id = long("id").autoIncrement()
    var studentId = reference("student_id", StudentEntity.id)
    var time = time("time")
    var dayOfWeek = enumerationByName<DayOfWeek>("day_of_week", 64)

    override val primaryKey = PrimaryKey(id)
}