package ru.svetomsk.persistence.dao.impl

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import ru.svetomsk.model.BotUserDto
import ru.svetomsk.persistence.UserEntity
import ru.svetomsk.persistence.DatabaseFactory.dbQuery
import ru.svetomsk.persistence.dao.contract.UsersDaoFacade
import ru.svetomsk.persistence.dao.resultRowToBotUserDto
import java.lang.RuntimeException

class UsersDaoFacadeImpl : UsersDaoFacade {
    override suspend fun save(userDto: BotUserDto): BotUserDto {
        return dbQuery {
            val insertStatement = UserEntity.insert {
                it[chatId] = userDto.chatId
            }
            insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToBotUserDto)
                ?: throw RuntimeException("Unable to create user")
        }
    }

    override suspend fun allUsers(): List<BotUserDto> {
        return dbQuery {
            UserEntity
                .selectAll()
                .map(::resultRowToBotUserDto)
        }
    }

    override suspend fun findByChatId(chatId: Long): BotUserDto? {
        return dbQuery {
            UserEntity
                .select { UserEntity.chatId eq chatId }
                .map(::resultRowToBotUserDto)
                .singleOrNull()
        }
    }

    override suspend fun deleteUser(chatId: Long): Int {
        return dbQuery {
            UserEntity.deleteWhere { UserEntity.chatId eq chatId }
        }
    }

    override suspend fun deleteAllUsers(): Int {
        return dbQuery {
            UserEntity.deleteAll()
        }
    }
}