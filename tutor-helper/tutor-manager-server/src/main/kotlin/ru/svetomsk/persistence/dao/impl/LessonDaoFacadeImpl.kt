package ru.svetomsk.persistence.dao.impl

import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.toJavaLocalTime
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.update
import ru.svetomsk.model.LessonDto
import ru.svetomsk.persistence.DatabaseFactory.dbQuery
import ru.svetomsk.persistence.LessonEntity
import ru.svetomsk.persistence.StudentEntity
import ru.svetomsk.persistence.dao.contract.LessonDaoFacade
import ru.svetomsk.persistence.dao.resultRowToLessonDto

class LessonDaoFacadeImpl : LessonDaoFacade {
    override suspend fun addLesson(lessonDto: LessonDto): LessonDto {
        return dbQuery {
            val insertStatement = LessonEntity.insert {
                it[studentId] = lessonDto.studentId
                it[time] = lessonDto.time.toJavaLocalTime()
                it[dayOfWeek] = lessonDto.dayOfWeek
            }
            insertStatement.resultedValues?.singleOrNull()?.let(::resultRowToLessonDto)
                ?: throw RuntimeException("Unable to create user")
        }
    }

    override suspend fun removeLesson(lessonId: Long): Boolean {
        return dbQuery {
            LessonEntity.deleteWhere { id eq lessonId }
        } > 0
    }

    override suspend fun getLessonByDay(dayOfWeek: DayOfWeek): List<LessonDto> {
        return dbQuery {
            (LessonEntity innerJoin StudentEntity).selectAll()
                .map(::resultRowToLessonDto)
        }
    }

    override suspend fun updateLesson(lessonId: Long, lessonDto: LessonDto): Boolean {
        return dbQuery {
            LessonEntity.update({ LessonEntity.id eq lessonId })
            {
                it[time] = lessonDto.time.toJavaLocalTime()
                it[studentId] = lessonDto.studentId
                it[dayOfWeek] = lessonDto.dayOfWeek
            }
        } > 0
    }

    override suspend fun getAllLessons(): List<LessonDto> {
        return dbQuery {
            LessonEntity.selectAll()
                .map(::resultRowToLessonDto)
        }
    }
}