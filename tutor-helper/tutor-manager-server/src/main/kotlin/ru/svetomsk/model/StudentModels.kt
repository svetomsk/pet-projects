package ru.svetomsk.model

import kotlinx.serialization.Serializable

@Serializable
data class StudentDto(
    val id: Long = -1,
    var name: String = ""
)
