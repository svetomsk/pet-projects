package ru.svetomsk.model

import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.LocalTime
import kotlinx.serialization.Serializable

@Serializable
data class LessonDto(private val id: Long = -1, val time: LocalTime, val studentId: Long, val dayOfWeek: DayOfWeek)

@Serializable
data class ScheduleForDay(val dayOfWeek: DayOfWeek, val lessons: List<LessonDto>)

@Serializable
data class ScheduleForWeek(val lessons: Map<DayOfWeek, ScheduleForDay>)