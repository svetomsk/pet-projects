package ru.svetomsk.model

import kotlinx.serialization.Serializable


@Serializable
data class BotUserDto(val chatId: Long)
@Serializable
data class GetUsersResponse(val users: List<BotUserDto>)

@Serializable
data class DeleteUserResponse(val deleteResult: Boolean)