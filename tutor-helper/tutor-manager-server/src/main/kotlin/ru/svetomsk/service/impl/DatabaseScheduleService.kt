package ru.svetomsk.service.impl

import kotlinx.datetime.DayOfWeek
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.model.LessonDto
import ru.svetomsk.model.ScheduleForDay
import ru.svetomsk.model.ScheduleForWeek
import ru.svetomsk.persistence.dao.contract.LessonDaoFacade
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.utils.getCurrentDay
import ru.svetomsk.utils.getNextDay

class DatabaseScheduleService: IScheduleService, KoinComponent {
    private val lessonDaoFacade: LessonDaoFacade by inject()

    override suspend fun getScheduleForToday(): ScheduleForDay {
        val dayOfWeek = getCurrentDay()
        val lesson = lessonDaoFacade.getLessonByDay(dayOfWeek)
        return ScheduleForDay(dayOfWeek, lesson)
    }

    override suspend fun getScheduleForTomorrow(): ScheduleForDay {
        val dayOfWeek = getNextDay()
        val lessons = lessonDaoFacade.getLessonByDay(dayOfWeek)
        return ScheduleForDay(dayOfWeek, lessons)
    }

    override suspend fun getScheduleForWeek(): ScheduleForWeek {
        val schedules = mutableMapOf<DayOfWeek, ScheduleForDay>()

        for (day in DayOfWeek.values()) {
            val lesson = lessonDaoFacade.getLessonByDay(day)
            schedules[day] = ScheduleForDay(day, lesson)
        }
        return ScheduleForWeek(schedules)
    }

    override suspend fun addLesson(lessonDto: LessonDto) {
        lessonDaoFacade.addLesson(lessonDto)
    }

    override suspend fun removeLesson(lessonId: Long): Boolean {
        return lessonDaoFacade.removeLesson(lessonId)
    }

    override suspend fun updateLesson(lessonId: Long, lessonDto: LessonDto): Boolean {
        return lessonDaoFacade.updateLesson(lessonId, lessonDto)
    }

    override suspend fun getAllLessons(): List<LessonDto> {
        return lessonDaoFacade.getAllLessons()
    }
}