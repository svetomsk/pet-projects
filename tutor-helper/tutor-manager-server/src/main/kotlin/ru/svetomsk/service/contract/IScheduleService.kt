package ru.svetomsk.service.contract

import ru.svetomsk.model.LessonDto
import ru.svetomsk.model.ScheduleForDay
import ru.svetomsk.model.ScheduleForWeek

interface IScheduleService {
    suspend fun getScheduleForToday(): ScheduleForDay

    suspend fun getScheduleForTomorrow(): ScheduleForDay

    suspend fun getScheduleForWeek(): ScheduleForWeek

    suspend fun addLesson(lessonDto: LessonDto)

    suspend fun removeLesson(lessonId: Long): Boolean

    suspend fun updateLesson(lessonId: Long, lessonDto: LessonDto): Boolean
    suspend fun getAllLessons(): List<LessonDto>
}