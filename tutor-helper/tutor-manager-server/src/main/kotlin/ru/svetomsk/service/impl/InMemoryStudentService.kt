package ru.svetomsk.service.impl

import ru.svetomsk.model.StudentDto
import ru.svetomsk.routes.GetStudentsResponse
import ru.svetomsk.service.contract.IStudentService

class InMemoryStudentService: IStudentService {
    private val studentDtos = mutableListOf<StudentDto>()

    override suspend fun getStudents(): GetStudentsResponse {
        return GetStudentsResponse(studentDtos)
    }

    override suspend fun addStudent(studentDto: StudentDto) {
        studentDtos.add(studentDto)
    }

    override suspend fun deleteStudent(studentId: Long): Boolean {
        return studentDtos.removeIf { it.id == studentId }
    }

    override suspend fun findStudent(name: String): List<StudentDto> {
        return studentDtos.filter { it.name == name }
    }

    override suspend fun getStudentsCount(): Int {
        return studentDtos.size
    }

    override suspend fun updateStudent(studentId: Long, studentDto: StudentDto): Boolean {
        studentDtos.find { it.id == studentId }?.run {
            name = studentDto.name
            return true
        }
        return false
    }

    override suspend fun findStudent(studentId: Long): StudentDto? {
        return studentDtos.find { it.id == studentId }
    }
}