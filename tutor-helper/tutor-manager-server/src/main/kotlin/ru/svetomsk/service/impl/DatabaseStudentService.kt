package ru.svetomsk.service.impl

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import ru.svetomsk.model.StudentDto
import ru.svetomsk.persistence.dao.contract.StudentDaoFacade
import ru.svetomsk.routes.GetStudentsResponse
import ru.svetomsk.service.contract.IStudentService

class DatabaseStudentService: IStudentService, KoinComponent {
    private val studentDao: StudentDaoFacade by inject()
    override suspend fun getStudents(): GetStudentsResponse {
        return GetStudentsResponse(studentDao.findAllStudents())
    }

    override suspend fun addStudent(studentDto: StudentDto) {
        studentDao.addStudent(studentDto)
    }

    override suspend fun deleteStudent(studentId: Long): Boolean {
        return studentDao.deleteStudent(studentId)
    }

    override suspend fun findStudent(name: String): List<StudentDto> {
        return studentDao.findStudentsByName(name)
    }

    override suspend fun getStudentsCount(): Int {
        TODO("Not yet implemented")
    }

    override suspend fun updateStudent(studentId: Long, studentDto: StudentDto): Boolean {
        return studentDao.updateStudent(studentId, studentDto)
    }

    override suspend fun findStudent(studentId: Long): StudentDto? {
        return studentDao.findStudentById(studentId)
    }
}