package ru.svetomsk.service.contract

import ru.svetomsk.model.StudentDto
import ru.svetomsk.routes.GetStudentsResponse

interface IStudentService {
    suspend fun getStudents(): GetStudentsResponse
    suspend fun addStudent(studentDto: StudentDto)
    suspend fun deleteStudent(studentId: Long): Boolean
    suspend fun findStudent(name: String): List<StudentDto>
    suspend fun getStudentsCount(): Int
    suspend fun updateStudent(studentId: Long, studentDto: StudentDto): Boolean
    suspend fun findStudent(studentId: Long): StudentDto?
}