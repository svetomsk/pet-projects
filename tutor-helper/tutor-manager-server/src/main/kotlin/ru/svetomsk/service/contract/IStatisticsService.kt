package ru.svetomsk.service.contract

interface IStatisticsService {
    fun markLessonAsComplete(lessonId: Long)
    fun markLessonAsSkipped(lessonId: Long)
}