package ru.svetomsk.service.impl

import kotlinx.datetime.DayOfWeek
import ru.svetomsk.model.LessonDto
import ru.svetomsk.model.ScheduleForDay
import ru.svetomsk.model.ScheduleForWeek
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.utils.getCurrentDay
import ru.svetomsk.utils.getNextDay

class FixedScheduleService: IScheduleService {
    private val schedule: Map<DayOfWeek, List<LessonDto>> = createSchedule()

    override suspend fun getScheduleForToday(): ScheduleForDay {
        val currentDay = getCurrentDay()
        schedule[currentDay]?.run {
            return ScheduleForDay(currentDay, this)
        } ?: return ScheduleForDay(currentDay, listOf())
    }

    override suspend fun getScheduleForTomorrow(): ScheduleForDay {
        val nextDay = getNextDay()
        schedule[nextDay]?.run {
            return ScheduleForDay(nextDay, this)
        } ?: return ScheduleForDay(nextDay, listOf())
    }

    override suspend fun getScheduleForWeek(): ScheduleForWeek {
        val schedules = mutableMapOf<DayOfWeek, ScheduleForDay>()

        for (day in schedule.keys) {
            schedules[day] = ScheduleForDay(day, schedule.getValue(day))
        }
        return ScheduleForWeek(schedules)
    }

    override suspend fun addLesson(lessonDto: LessonDto) {
        TODO("Not yet implemented")
    }

    override suspend fun removeLesson(lessonId: Long): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun updateLesson(lessonId: Long, lessonDto: LessonDto): Boolean {
        TODO("Not yet implemented")
    }

    override suspend fun getAllLessons(): List<LessonDto> {
        TODO("Not yet implemented")
    }

    private fun createSchedule(): MutableMap<DayOfWeek, List<LessonDto>> {
        val fixedSchedule = mutableMapOf<DayOfWeek, List<LessonDto>>()
//        val roman = StudentDto(0, "Роман")
//        val artem = StudentDto(0, "Артем")
//        val egor = StudentDto(0, "Егор")
//        val artemiy = StudentDto(0, "Артемий")
//        val ilyasov = StudentDto(0, "Ильясов Артем")
//        val amaliya = StudentDto(0, "Амалия")
//        val akim = StudentDto(0, "Аким")
//        val teodor = StudentDto(0, "Теодор")
//        val roma = StudentDto(0, "Рома")
//        fixedSchedule[DayOfWeek.MONDAY] = listOf(
//            LessonDto(LocalTime(12, 0), roman),
//            LessonDto(LocalTime(18, 15), artem)
//        )
//        fixedSchedule[DayOfWeek.TUESDAY] = listOf(
//            LessonDto(LocalTime(12, 0), egor),
//            LessonDto(LocalTime(15, 45), artemiy),
//            LessonDto(LocalTime(19, 30), ilyasov)
//        )
//        fixedSchedule[DayOfWeek.WEDNESDAY] = listOf(
//            LessonDto(LocalTime(15, 45), amaliya),
//            LessonDto(LocalTime(18, 15), akim),
//            LessonDto(LocalTime(20, 45), teodor),
//            LessonDto(LocalTime(22, 0), roma)
//        )
//        fixedSchedule[DayOfWeek.THURSDAY] = listOf(
//            LessonDto(LocalTime(12, 0), roman),
//            LessonDto(LocalTime(15, 45), artemiy),
//            LessonDto(LocalTime(19, 30), ilyasov)
//        )
//        fixedSchedule[DayOfWeek.FRIDAY] = listOf(
//            LessonDto(LocalTime(12, 0), egor),
//            LessonDto(LocalTime(18, 15), artem),
//        )
//        fixedSchedule[DayOfWeek.SATURDAY] = listOf()
//        fixedSchedule[DayOfWeek.SUNDAY] = listOf(LessonDto(LocalTime(14, 30), roman))

        return fixedSchedule
    }
}