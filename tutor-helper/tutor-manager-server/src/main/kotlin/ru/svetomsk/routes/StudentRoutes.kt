package ru.svetomsk.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.Serializable
import org.koin.ktor.ext.inject
import ru.svetomsk.model.StudentDto
import ru.svetomsk.service.contract.IStudentService
import ru.svetomsk.utils.getPathVariable
import ru.svetomsk.utils.getRequiredQueryParameter

@Serializable
data class GetStudentsResponse(val students: List<StudentDto>)

fun Route.studentsRoutes() {
    val service: IStudentService by inject()

    route("/students") {
        get {
            call.respond(service.getStudents())
        }

        put {
            val studentDto = call.receive<StudentDto>()
            call.respond(service.addStudent(studentDto))
        }

        get("/find") {
            val nameParam = call.getRequiredQueryParameter("name")
            call.respond(service.findStudent(nameParam))
        }

        get("/{studentId?}") {
            val studentId = call.getPathVariable("studentId")
            service.findStudent(studentId.toLong())?.let {
                call.respond(it)
            } ?: call.respond(HttpStatusCode.NotFound)
        }

        delete("{studentId?}") {
            val studentId = call.getPathVariable("studentId")
            call.respond(service.deleteStudent(studentId.toLong()))
        }

        patch("/{studentId?}") {
            val studentId = call.getPathVariable("studentId")
            call.respond(service.updateStudent(studentId.toLong(), call.receive<StudentDto>()))
        }
    }
}