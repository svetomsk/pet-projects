package ru.svetomsk.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject
import ru.svetomsk.model.BotUserDto
import ru.svetomsk.model.DeleteUserResponse
import ru.svetomsk.model.GetUsersResponse
import ru.svetomsk.persistence.dao.contract.UsersDaoFacade

fun Route.userRoutes() {
    route("/users") {
        val usersDaoFacade: UsersDaoFacade by inject()
        post {
            val botUser = call.receive<BotUserDto>()
            call.respond(HttpStatusCode.Created, usersDaoFacade.save(botUser))
        }

        get {
            call.respond(HttpStatusCode.OK, GetUsersResponse(usersDaoFacade.allUsers()))
        }

        delete("/{chatId?}") {
            val chatId = call.parameters["chatId"] ?: throw RuntimeException("Chat id not provided")
            usersDaoFacade.deleteUser(chatId.toLong())
            call.respond(HttpStatusCode.OK, DeleteUserResponse(true))
        }

        delete {
            usersDaoFacade.deleteAllUsers()
            call.respond(HttpStatusCode.OK, DeleteUserResponse(true))
        }
    }
}