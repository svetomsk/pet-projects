package ru.svetomsk.routes

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.koin.ktor.ext.inject
import ru.svetomsk.model.LessonDto
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.utils.getPathVariable

fun Route.scheduleRoutes() {
    val scheduleService: IScheduleService by inject()

    route("/schedule") {
        get("/today") {
            call.respond(HttpStatusCode.OK, scheduleService.getScheduleForToday())
        }

        get("/tomorrow") {
            call.respond(HttpStatusCode.OK, scheduleService.getScheduleForTomorrow())
        }

        get("/week") {
            call.respond(HttpStatusCode.OK, scheduleService.getScheduleForWeek())
        }

        put("/lesson") {
            scheduleService.addLesson(call.receive<LessonDto>())
            call.respond(HttpStatusCode.Created)
        }

        delete("/lesson/{lessonId?}") {
            val lessonId = call.getPathVariable("lessonId")
            call.respond(HttpStatusCode.OK, scheduleService.removeLesson(lessonId.toLong()))
        }

        patch("/lesson/{lessonId?}") {
            val lessonId = call.getPathVariable("lessonId")
            call.respond(scheduleService.updateLesson(lessonId.toLong(), call.receive<LessonDto>()))
        }

        get("lesson/list") {
            val allLessons = scheduleService.getAllLessons()
            call.respond(allLessons)
        }
    }
}