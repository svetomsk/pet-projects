package ru.svetomsk.utils

import io.ktor.server.application.*

fun ApplicationCall.getRequiredQueryParameter(paramName: String): String {
    return getOptionalQueryParameter(paramName) ?: throw RuntimeException("$paramName is required query parameter")
}

fun ApplicationCall.getOptionalQueryParameter(paramName: String): String? {
    return request.queryParameters[paramName]
}

fun ApplicationCall.getPathVariable(pathVariableName: String): String {
    return parameters[pathVariableName] ?: throw RuntimeException("$pathVariableName is required path variable")
}