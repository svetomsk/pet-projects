package ru.svetomsk.utils

import kotlinx.datetime.Clock
import kotlinx.datetime.DayOfWeek
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime

fun getCurrentDay(): DayOfWeek {
    val currentMoment = Clock.System.now()
    return currentMoment.toLocalDateTime(TimeZone.currentSystemDefault()).dayOfWeek
}

fun getNextDay(): DayOfWeek {
    val currentDay = getCurrentDay()
    return when (currentDay) {
        DayOfWeek.MONDAY -> DayOfWeek.TUESDAY
        DayOfWeek.TUESDAY -> DayOfWeek.WEDNESDAY
        DayOfWeek.WEDNESDAY -> DayOfWeek.THURSDAY
        DayOfWeek.THURSDAY -> DayOfWeek.FRIDAY
        DayOfWeek.FRIDAY -> DayOfWeek.SATURDAY
        DayOfWeek.SATURDAY -> DayOfWeek.SUNDAY
        DayOfWeek.SUNDAY -> DayOfWeek.MONDAY
    }
}