package ru.svetomsk

import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.koin.dsl.module
import org.koin.ktor.plugin.koin
import org.koin.logger.slf4jLogger
import ru.svetomsk.persistence.DatabaseFactory
import ru.svetomsk.persistence.persistenceModules
import ru.svetomsk.plugins.configureRouting
import ru.svetomsk.plugins.configureSerialization
import ru.svetomsk.service.contract.IScheduleService
import ru.svetomsk.service.contract.IStudentService
import ru.svetomsk.service.impl.DatabaseScheduleService
import ru.svetomsk.service.impl.DatabaseStudentService

val appModule = module {
    single<IStudentService> { DatabaseStudentService() }
    single<IScheduleService> { DatabaseScheduleService() }
}

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module,
        watchPaths = listOf("classes"))
        .start(wait = true)
}


fun Application.module() {
    DatabaseFactory.init()
    koin {
        slf4jLogger()
        modules(appModule, persistenceModules())
    }
    configureRouting()
    configureSerialization()
}
