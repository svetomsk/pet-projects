package ru.svetomsk

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.Serializable

@Serializable
data class Customer(val id: Int, val name: String)

val customerStorage = mutableListOf<Customer>()

fun Route.customerRouting() {
    route("/customer") {
        get {
            if (customerStorage.isEmpty()) {
                call.respondText("No customers", status = HttpStatusCode.OK)
            } else {
                call.respond(customerStorage)
            }
        }
        get("{id?}") {
            val id = call.parameters["id"] ?: return@get call.respondText(
                "Missing request param id",
                status = HttpStatusCode.BadRequest
            )
            val customer = customerStorage.find { it.id == id.toInt() }
                ?: return@get call.respondText("No customer with id $id", status = HttpStatusCode.NotFound)
            call.respond(customer)
        }
        post {
            val customer = call.receive<Customer>()
            customerStorage.add(customer)
            call.respondText("Customer added", status = HttpStatusCode.Created)
        }
        delete("{id?}") {
            val id = call.parameters["id"] ?: return@delete call.respond(HttpStatusCode.BadRequest)
            if (customerStorage.removeIf { it.id == id.toInt() }) {
                call.respondText("Customer with id $id was removed", status = HttpStatusCode.Accepted)
            } else {
                call.respondText("No customer with id $id", status = HttpStatusCode.NotFound)
            }
        }
    }

}