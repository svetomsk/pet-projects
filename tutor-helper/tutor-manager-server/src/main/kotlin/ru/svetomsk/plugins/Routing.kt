package ru.svetomsk.plugins

import io.ktor.server.application.*
import io.ktor.server.routing.*
import ru.svetomsk.customerRouting
import ru.svetomsk.routes.scheduleRoutes
import ru.svetomsk.routes.studentsRoutes
import ru.svetomsk.routes.userRoutes

fun Application.configureRouting() {
    routing {
        customerRouting()
        studentsRoutes()
        scheduleRoutes()
        userRoutes()
    }
}
